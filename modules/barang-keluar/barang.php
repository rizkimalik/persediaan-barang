<?php
session_start();

// Panggil koneksi database.php untuk koneksi database
require_once "../../config/database.php";


if (isset($_POST['get_gudang'])) {
    $query_barang = mysqli_query($mysqli, "SELECT b.id_barang,b.kode_barang,b.nama_barang
                                            FROM is_barang_masuk as a 
                                            INNER JOIN is_barang as b 
                                            INNER JOIN is_gudang as e
                                                ON a.id_barang=b.id_barang 
                                                AND a.id_gudang=e.id_gudang
                                                WHERE a.id_gudang='$_POST[id_gudang]'
                                            GROUP BY b.id_barang,b.nama_barang
                                            ORDER BY b.id_barang ASC")
        or die('Ada kesalahan pada query tampil barang: ' . mysqli_error($mysqli));
    $data = [];
    while ($data_barang = mysqli_fetch_assoc($query_barang)) {
        $data[] = $data_barang;
    }
    echo json_encode($data);
}

if (isset($_POST['get_barang'])) {
    $query_barang = mysqli_query($mysqli, "SELECT
                                            c.jumlah_masuk,
                                            c.jumlah_keluar,
                                            a.id_barang,
                                            d.id_rak,
                                            d.kode_rak,
                                            d.nama_rak
                                        FROM is_barang_masuk AS a
                                            LEFT JOIN view_barang AS b ON a.id_barang = b.id_barang
                                            LEFT JOIN view_stok AS c ON a.id_barang = c.id_barang AND c.id_gudang=a.id_gudang
                                            LEFT JOIN is_rak AS d ON d.id_rak = a.id_rak
                                        WHERE a.id_barang = '$_POST[id_barang]' AND a.id_gudang = '$_POST[id_gudang]'
                                        GROUP BY
                                            c.jumlah_masuk,
                                            c.jumlah_keluar,
                                            a.id_barang,
                                            d.id_rak,
                                            d.kode_rak,
                                            d.nama_rak")
        or die('Ada kesalahan pada query tampil barang: ' . mysqli_error($mysqli));
    $data_barang = mysqli_fetch_assoc($query_barang);
    $stok = $data_barang['jumlah_masuk'] - $data_barang['jumlah_keluar'];
    $data_barang['stok'] = $stok;
    echo json_encode($data_barang);
}
