<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <i class="fa fa-file-text-o icon-title"></i> Laporan Stok Barang
        <div class="pull-right">
            <div class="btn-group">
                <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Gudang
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="?module=lap_stok">Semua Data</a></li>
                    <li class="divider"></li>
                    <?php
                    $query_gudang = mysqli_query($mysqli, "SELECT id_gudang,nama_gudang FROM is_gudang ORDER BY id_gudang ASC")
                        or die('Ada kesalahan pada query tampil Gudang: ' . mysqli_error($mysqli));
                    while ($data_gudang = mysqli_fetch_assoc($query_gudang)) {
                        echo "<li><a href='?module=lap_stok&gudang=$data_gudang[id_gudang]'>$data_gudang[nama_gudang]</a></li>";
                    }
                    ?>
                </ul>
            </div>
            <a class="btn btn-primary btn-social" href="modules/lap-stok/cetak.php" target="_blank">
                <i class="fa fa-print"></i> Cetak
            </a>
        </div>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                    <!-- tampilan tabel barang -->
                    <table id="dataTables1" class="table table-bordered table-striped table-hover">
                        <!-- tampilan tabel header -->
                        <thead>
                            <tr>
                                <th class="center">No.</th>
                                <th class="center">ID Barang</th>
                                <th class="center">Kode Barang</th>
                                <th class="center">Nama Barang</th>
                                <th class="center">Jenis Barang</th>
                                <th class="center">Harga Barang</th>
                                <th class="center">Stok</th>
                                <th class="center">Satuan</th>
                            </tr>
                        </thead>
                        <!-- tampilan tabel body -->
                        <tbody>
                            <?php
                            $no = 1;
                            $gudang = "";
                            // fungsi query untuk menampilkan data dari tabel barang
                            if (isset($_GET['gudang'])) {
                                $gudang = "AND id_gudang='$_GET[gudang]'";
                            }

                            $query = mysqli_query($mysqli, "SELECT * FROM view_barang ORDER BY id_barang ASC")
                                or die('Ada kesalahan pada query tampil Data Barang: ' . mysqli_error($mysqli));

                            // tampilkan data
                            while ($data = mysqli_fetch_assoc($query)) {
                                $query_stok = mysqli_query($mysqli, "SELECT id_barang,IFNULL( SUM( jumlah_masuk ), 0 ) as jumlah_masuk, IFNULL( SUM( jumlah_keluar ), 0 ) as jumlah_keluar from view_stok WHERE id_barang='$data[id_barang]' $gudang GROUP BY id_barang");
                                $data_stok = mysqli_fetch_assoc($query_stok);
                                
                                $jumlah_masuk = isset($data_stok['jumlah_masuk']) ? $data_stok['jumlah_masuk'] : 0;
                                $jumlah_keluar = isset($data_stok['jumlah_keluar']) ? $data_stok['jumlah_keluar'] : 0;
                                $stok = $jumlah_masuk - $jumlah_keluar;

                                // menampilkan isi tabel dari database ke tabel di aplikasi
                                echo "<tr>
                                <td width='30' class='center'>$no</td>
                                <td width='80' class='center'>$data[id_barang]</td>
                                <td width='80' class='center'>$data[kode_barang]</td>
                                <td width='200'>$data[nama_barang]</td>
                                <td width='170'>$data[nama_jenis]</td>
                                <td width='170'>Rp. " . number_format($data['harga_barang'], 2) . "</td>";
                                            if ($stok <= 1000) { ?>
                                                <td width="80" align="right"><span class="label label-warning"><?php echo $stok; ?></span></td>
                                            <?php
                                            } else { ?>
                                                <td width="80" align="right"><?php echo $stok; ?></td>
                                        <?php
                                            }
                                            echo "   <td width='100'>$data[nama_satuan]</td>
                                </tr>";
                                $no++;
                            }
                            ?>
                        </tbody>
                    </table>
                    <div>
                        <strong>Keterangan :</strong> <br>
                        <span style="color:#f0ad4e" class="label label-warning">...</span> = Stok Barang Minim
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <!--/.col -->
    </div> <!-- /.row -->
</section><!-- /.content